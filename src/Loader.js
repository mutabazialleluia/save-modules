import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  activityIndicator: {
    marginTop: 50,
  },
});

const Loader = ({ size, loaderStyles }) => (
  <ActivityIndicator
    size={size}
    style={{ ...styles.activityIndicator, ...loaderStyles }}
  />
);

Loader.propTypes = {
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  loaderStyles: PropTypes.instanceOf(Object),
};

Loader.defaultProps = {
  size: 'large',
  loaderStyles: {},
};

export default Loader;
