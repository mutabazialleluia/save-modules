import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  sublistItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    paddingHorizontal: dimensions.padding,
    paddingTop: 15,
  },
  warningText: {
    color: colors.red,
  },
  infoText: {
    color: colors.green,
  },
});

const SubListItem = ({
  leftText,
  rightText,
  info,
  warning,
  dimensions,
  colors,
}) => {
  const styles = _styles({ dimensions, colors });

  const getTextStyle = () => {
    if (warning) {
      return styles.warningText;
    }
    if (info) {
      return styles.infoText;
    }
    return {};
  };

  return (
    <View style={styles.sublistItem}>
      <Text style={getTextStyle()}>{leftText}</Text>
      <Text style={getTextStyle()}>{rightText}</Text>
    </View>
  );
};

SubListItem.defaultProps = {
  leftText: null,
  rightText: null,
  info: false,
  warning: false,
  dimensions: {},
  colors: {},
};
SubListItem.propTypes = {
  leftText: PropTypes.string,
  rightText: PropTypes.string,
  info: PropTypes.bool,
  warning: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

export default SubListItem;
