import React, { useState, useEffect } from 'react';
import {
  Modal as NativeModal,
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const _style = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backdrop: {
    position: 'absolute',
    backgroundColor: colors.blackFaded,
    height: '100%',
    width: '100%',
    top: 0,
    left: 0,
  },
  inner: {
    maxWidth: Dimensions.get('window').width - dimensions.padding * 4,
    maxHeight: Dimensions.get('window').height - dimensions.padding * 6,
    minWidth: Dimensions.get('window').width - dimensions.padding * 4,
    backgroundColor: colors.white,
    borderRadius: 4,
    flexDirection: 'column',
    padding: dimensions.padding,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: dimensions.padding * 1.6,
    fontWeight: '800',
    paddingBottom: dimensions.padding,
    textAlign: 'center',
  },
  message: {
    fontSize: dimensions.padding,
    paddingBottom: dimensions.padding,
    textAlign: 'center',
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
  },
  button: {
    height: dimensions.padding * 3,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 4,
  },
  no: {
    borderColor: colors.red,
    marginRight: dimensions.padding / 2,
  },
  yes: {
    borderColor: colors.green,
    marginLeft: dimensions.padding / 2,
  },
  disabled: { borderColor: colors.gray },
  buttonText: {
    fontSize: dimensions.padding,
  },
  noText: {
    color: colors.red,
  },
  yesText: {
    color: colors.green,
  },
  errorText: {
    paddingBottom: 20,
    color: colors.red,
  },
  disabledText: { color: colors.gray },
});

const Confirm = ({
  title,
  message,
  onYesPress,
  onNoPress,
  yesText,
  noText,
  trigger,
  show,
  loading,
  hasError,
  dimensions,
  colors,
}) => {
  const [showLocal, setShowLocal] = useState(show);

  const style = _style({ dimensions, colors });

  useEffect(() => {
    setShowLocal(show);
  }, [show]);

  return (
    <>
      {trigger
        && React.cloneElement(trigger, {
          onPress: () => {
            setShowLocal(true);
          },
        })}
      <NativeModal animationType="slide" visible={showLocal} transparent>
        <View style={style.wrapper}>
          <TouchableOpacity
            style={style.backdrop}
            disabled={loading}
            onPress={() => {
              onNoPress();
              setShowLocal(false);
            }}
          />

          <View style={style.inner}>
            {title && (
              <Text style={[style.title, loading ? style.disabledText : {}]}>
                {title}
              </Text>
            )}
            {!loading && (
              <Text
                style={[
                  hasError ? style.errorText : style.message,
                  loading ? style.disabledText : {},
                ]}
              >
                {message}
              </Text>
            )}
            {loading && <ActivityIndicator size="large" color={colors.blue} />}
            <View style={style.actions}>
              <TouchableOpacity
                style={[style.button, style.no, loading ? style.disabled : {}]}
                disabled={loading}
                onPress={() => {
                  onNoPress();
                  setShowLocal(false);
                }}
              >
                <Text
                  style={[
                    style.buttonText,
                    style.noText,
                    loading ? style.disabledText : {},
                  ]}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {noText}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[style.button, style.yes, loading ? style.disabled : {}]}
                disabled={loading}
                onPress={() => {
                  onYesPress();
                }}
              >
                <Text
                  style={[
                    style.buttonText,
                    style.yesText,
                    loading ? style.disabledText : {},
                  ]}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {yesText}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </NativeModal>
    </>
  );
};

Confirm.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  trigger: PropTypes.node,
  onYesPress: PropTypes.func,
  onNoPress: PropTypes.func,
  yesText: PropTypes.string,
  noText: PropTypes.string,
  show: PropTypes.bool,
  loading: PropTypes.bool,
  hasError: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Confirm.defaultProps = {
  title: null,
  message: null,
  trigger: null,
  onYesPress: () => {},
  onNoPress: () => {},
  yesText: 'yes',
  noText: 'no',
  show: false,
  hasError: false,
  loading: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Confirm);
