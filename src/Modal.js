import React, { useState, useEffect, useCallback } from 'react';
import {
  Modal as NativeModal,
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const _style = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backdrop: {
    position: 'absolute',
    backgroundColor: colors.blackFaded,
    height: '100%',
    width: '100%',
    top: 0,
    left: 0,
  },
  inner: {
    width: Dimensions.get('window').width - dimensions.padding * 4,
    maxHeight: Dimensions.get('window').height - dimensions.padding * 6,
    backgroundColor: colors.white,
    borderRadius: 4,
    flexDirection: 'column',
  },
  content: {
    padding: dimensions.padding,
  },
  header: {
    flexDirection: 'row',
    paddingLeft: dimensions.padding,
    alignItems: 'center',
    borderBottomColor: colors.gray,
    borderBottomWidth: 1,
  },
  title: { flex: 1 },
  titleText: {
    fontWeight: 'bold',
    color: colors.blue,
  },
  close: {
    height: dimensions.padding * 3,
    paddingHorizontal: dimensions.padding,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeDisabled: { opacity: 0.3 },
  loader: { marginLeft: dimensions.padding },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: dimensions.padding,
    borderTopColor: colors.gray,
    borderTopWidth: 1,
  },
});
const Modal = ({
  open,
  title,
  headerExtra,
  trigger,
  beforeTrigger,
  onHide,
  footer,
  children,
  close,
  loading,
  dimensions,
  colors,
}) => {
  const [show, setShow] = useState(false);

  const ClsFN = useCallback(() => {
    onHide();
    setShow(false);
  }, [onHide]);

  close(ClsFN);

  useEffect(() => {
    setShow(open);
  }, [open]);

  const style = _style({ dimensions, colors });

  return (
    <>
      {React.cloneElement(trigger, {
        onPress: () => {
          if (beforeTrigger()) {
            setShow(true);
          }
        },
      })}
      <NativeModal
        animationType="slide"
        visible={show}
        transparent
        onDismiss={() => onHide()}
      >
        <View style={style.wrapper}>
          <TouchableOpacity style={style.backdrop} onPress={() => ClsFN()} />
          <View style={style.inner}>
            <View style={style.header}>
              <View style={style.title}>
                <Text
                  style={style.titleText}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {title}
                </Text>
              </View>
              {headerExtra}
              {loading && (
                <ActivityIndicator
                  style={style.loader}
                  color={colors.blackFaded}
                  size="small"
                />
              )}
              <TouchableOpacity
                disabled={loading}
                style={[style.close, loading ? style.closeDisabled : {}]}
                onPress={() => ClsFN()}
              >
                <Icon name="x" size={dimensions.padding} />
              </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={style.content}>{children}</View>
            </ScrollView>
            {footer && <View style={style.footer}>{footer}</View>}
          </View>
        </View>
      </NativeModal>
    </>
  );
};
Modal.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  headerExtra: PropTypes.node,
  trigger: PropTypes.node,
  beforeTrigger: PropTypes.func,
  onHide: PropTypes.func,
  footer: PropTypes.node,
  children: PropTypes.node,
  close: PropTypes.func,
  loading: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};
Modal.defaultProps = {
  open: false,
  title: '',
  headerExtra: null,
  trigger: <View />,
  beforeTrigger: () => true,
  onHide: () => {},
  footer: null,
  children: null,
  close: () => {},
  loading: false,
  dimensions: {},
  colors: {},
};
export default withTheme(Modal);
