/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import { withTheme } from '../utils/theme';

const _styleSheet = ({ colors, dimensions }) => StyleSheet.create({
  button: {
    backgroundColor: colors.white,
    height: dimensions.inputHeight,
    paddingHorizontal: dimensions.padding,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  smallButton: {
    height: dimensions.inputHeight * 0.65,
  },
  block: {
    width: '100%',
    alignItems: 'center',
  },
  danger: { backgroundColor: colors.red },
  primary: { backgroundColor: colors.blue },
  success: { backgroundColor: colors.green },
  disabled: {
    opacity: 0.3,
  },
  indicator: {
    position: 'absolute',
    marginLeft: '40%',
  },
  text: {
    lineHeight: dimensions.inputHeight,
    fontWeight: '600',
    color: colors.blue,
    fontSize: dimensions.padding,
    opacity: 1,
    alignSelf: 'center',
  },
  smallText: {
    lineHeight: dimensions.inputHeight * 0.65,
    fontSize: dimensions.padding * 0.65,
  },
  textLight: {
    color: colors.white,
  },
  textLoading: { opacity: 0.05 },
  iconLeft: {
    marginRight: dimensions.padding / 2,
  },
  iconRight: {
    marginRight: 0,
    marginLeft: dimensions.padding / 2,
  },
});

const Ic = ({
  icon,
  iconRight,
  children,
  small,
  danger,
  primary,
  success,
  dimensions,
  colors,
}) => {
  const styleSheet = _styleSheet({ dimensions, colors });

  return (
    icon && (
      <Icon
        name={icon}
        style={[
          children ? styleSheet.iconLeft : {},
          iconRight ? styleSheet.iconRight : {},
        ]}
        size={
          small
            ? styleSheet.smallText.lineHeight * 0.6
            : styleSheet.text.lineHeight * 0.6
        }
        color={
          danger || primary || success
            ? styleSheet.textLight.color
            : styleSheet.text.color
        }
      />
    )
  );
};

const CustomButton = (props) => {
  const {
    disabled,
    danger,
    primary,
    success,
    block,
    small,
    loading,
    style,
    children,
    icon,
    iconRight,
    onPress,
    dimensions,
    colors,
  } = props;

  const styleSheet = _styleSheet({ dimensions, colors });

  return (
    <TouchableOpacity
      {...props}
      disabled={onPress === null || loading || disabled}
      style={[
        styleSheet.button,
        small ? styleSheet.smallButton : {},
        danger ? styleSheet.danger : {},
        primary ? styleSheet.primary : {},
        success ? styleSheet.success : {},
        disabled ? styleSheet.disabled : {},
        block ? styleSheet.block : {},
        style,
      ]}
    >
      {!iconRight && (
        <Ic
          {...{
            icon,
            iconRight,
            children,
            small,
            danger,
            primary,
            success,
            dimensions,
            colors,
          }}
        />
      )}
      <Text
        style={[
          styleSheet.text,
          small ? styleSheet.smallText : {},
          danger || primary || success ? styleSheet.textLight : {},
          loading ? styleSheet.textLoading : {},
        ]}
      >
        {children}
      </Text>
      {iconRight && (
        <Ic
          {...{
            icon,
            iconRight,
            children,
            small,
            danger,
            primary,
            success,
            dimensions,
            colors,
          }}
        />
      )}
      <ActivityIndicator
        animating={loading}
        style={styleSheet.indicator}
        size={small ? dimensions.inputHeight * 0.65 : dimensions.inputHeight}
        color={danger || primary || success ? colors.white : colors.blue}
      />
    </TouchableOpacity>
  );
};

CustomButton.propTypes = {
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  danger: PropTypes.bool,
  primary: PropTypes.bool,
  success: PropTypes.bool,
  block: PropTypes.bool,
  small: PropTypes.bool,
  loading: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object]),
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  icon: PropTypes.string,
  iconRight: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

CustomButton.defaultProps = {
  onPress: null,
  disabled: false,
  danger: false,
  primary: false,
  success: false,
  block: false,
  small: false,
  loading: false,
  style: {},
  children: null,
  icon: null,
  iconRight: false,
  dimensions: {},
  colors: {},
};

export default withTheme(CustomButton);
