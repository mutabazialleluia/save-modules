import React from 'react';
import PropTypes from 'prop-types';

import {
  View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';
import { withTheme } from './utils/theme';

const _styleSheet = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    padding: dimensions.padding,
    backgroundColor: colors.white,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  wapperWarning: { backgroundColor: colors.yellow },
  wapperError: { backgroundColor: colors.red },
  wapperSuccess: { backgroundColor: colors.green },
  ribbon: { borderRadius: 0 },
  inner: {
    flex: 1,
    flexDirection: 'column',
  },
  textLight: {
    color: colors.white,
  },
  title: {
    fontSize: dimensions.padding,
    fontWeight: '600',
    color: colors.black,
  },
  message: {
    fontSize: dimensions.padding * 0.8,
    color: colors.gray,
  },
  action: {
    borderLeftColor: colors.gray,
    borderLeftWidth: 1,
    paddingLeft: dimensions.padding,
    marginLeft: dimensions.padding,
  },
  actionLight: { borderLeftColor: colors.whiteFaded },
  actionTitle: {
    fontSize: dimensions.padding,
    fontWeight: '600',
    color: colors.blue,
  },
});

const Message = ({
  visible,
  success,
  error,
  warning,
  action,
  title,
  message,
  style,
  ribbon,
  dimensions,
  colors,
}) => {
  const styleSheet = _styleSheet({ dimensions, colors });
  return !visible ? null : (
    <View
      style={[
        styleSheet.wrapper,
        warning ? styleSheet.wapperWarning : {},
        error ? styleSheet.wapperError : {},
        success ? styleSheet.wapperSuccess : {},
        ribbon ? styleSheet.ribbon : {},
        style,
      ]}
    >
      <View style={styleSheet.inner}>
        {title && (
          <Text
            style={[
              styleSheet.title,
              warning || error || success ? styleSheet.textLight : {},
            ]}
          >
            {title}
          </Text>
        )}
        {message && (
          <Text
            style={[
              styleSheet.message,
              warning || error || success ? styleSheet.textLight : {},
            ]}
          >
            {message}
          </Text>
        )}
      </View>
      {action && (
        <TouchableOpacity
          style={[
            styleSheet.action,
            warning || error || success ? styleSheet.actionLight : {},
          ]}
          onPress={action.onPress}
        >
          <Text
            style={[
              styleSheet.actionTitle,
              warning || error || success ? styleSheet.textLight : {},
            ]}
          >
            {action.title.toUpperCase()}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

Message.propTypes = {
  visible: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.bool,
  warning: PropTypes.bool,
  action: PropTypes.oneOfType([PropTypes.object]),
  title: PropTypes.string,
  message: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object]),
  ribbon: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Message.defaultProps = {
  visible: false,
  success: false,
  error: false,
  warning: false,
  action: null,
  title: null,
  message: null,
  style: {},
  ribbon: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Message);
