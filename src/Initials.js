import React from 'react';
import {
  View, Text, StyleSheet, ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const transform = (name, single) => {
  let str = '';

  const arr = name.trim().split(' ');

  if (single) {
    str = arr[0].charAt(0);
  } else if (arr.length === 1) {
    str = arr[0].charAt(0) + arr[0].charAt(1);
  } else {
    str = arr[0].charAt(0) + arr[1].charAt(0);
  }

  return str.toUpperCase();
};

const _styleSheet = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    height: dimensions.padding * 3,
    width: dimensions.padding * 3,
    borderRadius: dimensions.padding * 1.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.white,
    fontWeight: '800',
    fontSize: dimensions.padding * 1.3,
  },
});

const Initials = ({
  single, name, color, style, dimensions, colors,
}) => {
  const styleSheet = _styleSheet({ dimensions, colors });
  return (
    <View
      style={[
        styleSheet.wrapper,
        { backgroundColor: color || colors.darkBlue },
        style,
      ]}
    >
      <Text style={styleSheet.text}>{transform(name || 'User', single)}</Text>
    </View>
  );
};

Initials.propTypes = {
  single: PropTypes.bool,
  name: PropTypes.string,
  color: PropTypes.string,
  style: ViewPropTypes.style,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Initials.defaultProps = {
  single: false,
  name: null,
  color: null,
  style: {},
  dimensions: {},
  colors: {},
};

export default withTheme(Initials);
