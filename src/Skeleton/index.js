import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';

import Header from '../Header';
import Tabs from './tabs';
import Toast from './toast';
import Badge from '../Badge';
import Button from '../Button';
import { withTheme } from '../utils/theme';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: { flex: 1, flexDirection: 'column' },
  content: { flex: 1 },
  toastsWrapper: {
    position: 'absolute',
    zIndex: 999,
    top: 0,
    width: '100%',
    left: 0,
    right: 0,
  },
  action: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    zIndex: 999,
  },
  actionButton: {
    marginBottom: dimensions.padding,
    marginRight: dimensions.padding,
    backgroundColor: colors.primary,
    height: dimensions.toolbarHeight,
    width: dimensions.toolbarHeight,
    borderRadius: dimensions.toolbarHeight / 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2.62,
    elevation: 4,
  },
  actionButtonIcon: {
    color: colors.white,
  },
});

const Skeleton = ({
  title,
  subtitle,
  children,
  top,
  scroll,
  headerLeft,
  headerRight,
  tabs,
  toast,
  navigation,
  noTabBar,
  noSubtitle,
  extraHeader,
  change,
  flatList,
  action,
  dimensions,
  colors,
}) => {
  const [t, setT] = useState(tabs.length === 0 ? subtitle : tabs[0].title);
  const [toasts, setToasts] = useState([]);

  const styles = _styles({ dimensions, colors });

  /**
   *
   * @param {String} title
   * @param {String} message
   * @param {String} type One of "info", "error" and "success"
   * @param {Number} duration In seconds
   */
  const tst = ({
    toastTitle, message, type = 'info', duration = 5,
  }) => {
    setToasts([
      ...toasts,
      {
        toastTitle,
        message,
        type,
        key: `toast-${toasts.length}-${Math.random()}`,
        duration,
      },
    ]);
  };

  useEffect(() => {
    toast(tst);
  });

  const UseComponent = flatList ? View : ScrollView;

  return (
    <View style={styles.wrapper}>
      <StatusBar backgroundColor={colors.darkBlue} barStyle="default" />
      <Header
        title={title}
        subtitle={noSubtitle ? null : t}
        left={headerLeft}
        right={(
          <>
            {top && (
              <Badge text={9}>
                <Button
                  icon="bell"
                  primary
                  onPress={() => navigation.navigate('Notifications')}
                />
              </Badge>
            )}
            {headerRight}
          </>
        )}
        back={!top}
      />
      {extraHeader && <View>{extraHeader}</View>}
      {scroll && tabs.length === 0 ? (
        <UseComponent style={styles.content}>{children}</UseComponent>
      ) : (
        <View style={styles.content}>
          {tabs.length === 0 ? (
            children
          ) : (
            <Tabs
              tabs={tabs}
              setTitle={(value) => setT(value)}
              noTabBar={noTabBar}
              change={change}
              flatList={flatList}
              toast={tst}
            />
          )}
        </View>
      )}
      <SafeAreaView style={styles.toastsWrapper}>
        <FlatList
          data={toasts}
          renderItem={({ item }) => (
            <Toast
              toast={item}
              removeItem={(key) => {
                const newToasts = [];
                toasts.map((i) => {
                  if (i.key !== key) {
                    newToasts.push(i);
                  }
                  return i;
                });
                setToasts(newToasts);
              }}
            />
          )}
        />
      </SafeAreaView>
      {action && (
        <SafeAreaView style={styles.action}>
          <TouchableOpacity
            style={[
              styles.actionButton,
              {
                backgroundColor: action.color ? action.color : colors.blue,
              },
            ]}
            onPress={action.onPress}
          >
            <Icon
              name={action.icon}
              size={dimensions.tabIconSize * 0.8}
              style={styles.actionButtonIcon}
            />
          </TouchableOpacity>
        </SafeAreaView>
      )}
    </View>
  );
};

Skeleton.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  children: PropTypes.node,
  scroll: PropTypes.bool,
  top: PropTypes.bool,
  headerLeft: PropTypes.node,
  headerRight: PropTypes.node,
  tabs: PropTypes.oneOfType([PropTypes.array]),
  toast: PropTypes.func,
  navigation: PropTypes.oneOfType([PropTypes.object]),
  noSubtitle: PropTypes.bool,
  noTabBar: PropTypes.bool,
  extraHeader: PropTypes.node,
  change: PropTypes.func,
  flatList: PropTypes.bool,
  action: PropTypes.oneOfType([PropTypes.object]),
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Skeleton.defaultProps = {
  title: null,
  subtitle: null,
  children: null,
  scroll: true,
  top: false,
  headerLeft: null,
  headerRight: null,
  tabs: [],
  toast: () => {},
  noSubtitle: false,
  noTabBar: false,
  extraHeader: null,
  change: () => {},
  flatList: false,
  action: null,
  navigation: {},
  dimensions: {},
  colors: {},
};

export default withTheme(Skeleton);
