import React from 'react';
import {
  Animated,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import { withTheme } from '../utils/theme';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  toast: {
    width: dimensions.width,
    flexDirection: 'row',
    padding: dimensions.padding,
    backgroundColor: colors.white,
    borderBottomColor: colors.gray,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  toastTexts: {
    flex: 1,
    paddingHorizontal: dimensions.padding,
  },
  toastTitle: {
    fontSize: dimensions.homeCardFontSize * 1.2,
    fontWeight: '600',
  },
  toastMessage: {
    fontSize: dimensions.homeCardFontSize,
  },
  toastButtonIcon: {
    color: colors.littleDarkGray,
  },
});

const Toast = ({
  toast, removeItem, dimensions, colors,
}) => {
  const timer = setTimeout(() => {
    removeItem(toast.key);
  }, toast.duration * 1000);

  const styles = _styles({ dimensions, colors });

  return (
    <Animated.View style={styles.toast}>
      <Icon
        style={styles.toastIcon}
        name={
          {
            info: 'info',
            error: 'alert-triangle',
            success: 'check',
          }[toast.type]
        }
        color={
          {
            info: colors.blue,
            error: colors.red,
            success: colors.green,
          }[toast.type]
        }
        size={dimensions.tabIconSize}
      />
      <View style={styles.toastTexts}>
        <Text style={styles.toastTitle}>{toast.title}</Text>
        <Text style={styles.toastMessage}>{toast.message}</Text>
      </View>
      <TouchableOpacity
        style={styles.toastButton}
        onPress={() => {
          clearTimeout(timer);
          removeItem(toast.key);
        }}
      >
        <Icon
          name="x"
          style={styles.toastButtonIcon}
          size={dimensions.tabIconSize}
        />
      </TouchableOpacity>
    </Animated.View>
  );
};

Toast.propTypes = {
  toast: PropTypes.oneOfType([PropTypes.object]).isRequired,
  removeItem: PropTypes.func.isRequired,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Toast.defaultProps = {
  dimensions: {},
  colors: {},
};

export default withTheme(Toast);
