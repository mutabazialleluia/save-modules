import React, {
  useState, useRef, useEffect, useCallback,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import ViewPager from '@react-native-community/viewpager';
import Icon from 'react-native-vector-icons/Feather';
import { withTheme } from '../utils/theme';

const { width } = Dimensions.get('window');

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: { flex: 1, flexDirection: 'column' },
  pager: { flex: 1 },
  page: { flex: 1 },
  barWrapper: {
    backgroundColor: colors.blue,
  },
  bar: {
    height: dimensions.toolbarHeight,
    flexDirection: 'row',
    backgroundColor: colors.darkBlue,
  },
  barButton: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.blue,
  },
  barButtonTitle: {
    fontSize: dimensions.tabFontSize,
    color: colors.whiteSemiFaded,
  },
  barButtonTitleActive: {
    color: colors.white,
  },
  line: {
    height: 4,
    width,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: colors.white,
  },
});

const TabComp = ({ comp, toast }) => {
  const Comp = comp;
  return <Comp toast={toast} />;
};

TabComp.propTypes = {
  comp: PropTypes.func.isRequired,
  toast: PropTypes.func.isRequired,
};

const TabButton = ({
  title,
  icon,
  active,
  index,
  changeTab,
  dimensions,
  colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <TouchableOpacity
      style={[styles.barButton]}
      onPress={() => changeTab(index)}
    >
      <Icon
        size={dimensions.tabIconSize}
        color={active === index ? colors.white : colors.whiteSemiFaded}
        name={icon}
      />
      <Text
        style={[
          styles.barButtonTitle,
          active === index ? styles.barButtonTitleActive : {},
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

TabButton.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  active: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  changeTab: PropTypes.func.isRequired,
  dimensions: PropTypes.oneOfType([PropTypes.object]).isRequired,
  colors: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

const Tabs = ({
  tabs,
  setTitle,
  noTabBar,
  change,
  flatList,
  toast,
  dimensions,
  colors,
}) => {
  const [page, setPage] = useState(0);
  const [left] = useState(new Animated.Value(0));

  const styles = _styles({ dimensions, colors });

  useEffect(() => {
    Animated.timing(left, {
      toValue: (width / tabs.length) * page,
      delay: 100,
      duration: 300,
    }).start();
  }, [left, page, tabs]);

  let pager = useRef();

  const changePage = useCallback(
    (p) => {
      setPage(p);
      pager.setPage(p);
      setTitle(tabs[p].title);
    },
    [setTitle, tabs],
  );

  useEffect(() => {
    change(changePage);
  }, [change, changePage]);

  const UseComponent = flatList ? View : ScrollView;

  return (
    <View style={styles.wrapper}>
      <ViewPager
        style={styles.pager}
        onPageSelected={(e) => setPage(e.nativeEvent.position)}
        ref={(ref) => {
          pager = ref;
        }}
        scrollEnabled={false}
      >
        {tabs.map(({ title, component }, index) => (
          <UseComponent key={title} style={styles.page}>
            {page === index && <TabComp comp={component} toast={toast} />}
          </UseComponent>
        ))}
      </ViewPager>
      {!noTabBar && (
        <SafeAreaView style={styles.barWrapper}>
          <View style={styles.bar}>
            {tabs.map(({ title, icon }, index) => (
              <TabButton
                key={icon}
                title={title}
                icon={icon}
                active={page}
                index={index}
                changeTab={changePage}
                dimensions={dimensions}
                colors={colors}
              />
            ))}
            <Animated.View
              style={[
                styles.line,
                {
                  width: width / tabs.length,
                  left,
                },
              ]}
            />
          </View>
        </SafeAreaView>
      )}
    </View>
  );
};
Tabs.propTypes = {
  noTabBar: PropTypes.bool,
  tabs: PropTypes.oneOfType([PropTypes.array]).isRequired,
  setTitle: PropTypes.func.isRequired,
  change: PropTypes.func,
  flatList: PropTypes.bool,
  toast: PropTypes.func,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Tabs.defaultProps = {
  noTabBar: false,
  change: () => {},
  flatList: false,
  toast: () => {},
  dimensions: {},
  colors: {},
};

export default withTheme(Tabs);
