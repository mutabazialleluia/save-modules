import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ViewPropTypes,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import Initials from './Initials';

const _styleSheet = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    flexDirection: 'column',
  },
  wrapperInner: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: dimensions.padding,
  },
  contentWrapper: {
    flex: 1,
  },
  contentInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textWrapper: {
    flex: 1,
    paddingVertical: dimensions.padding,
    paddingRight: dimensions.padding,
    minHeight: dimensions.inputHeight,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  textWrapperBig: { minHeight: dimensions.inputHeight * 1.6 },
  textWrapperWithBottom: { paddingBottom: 0 },
  title: {
    fontWeight: '600',
    color: colors.black,
    fontSize: dimensions.listItemFontSize,
  },
  description: {
    color: colors.black,
    fontSize: dimensions.listItemFontSize * 0.8,
  },
  disabledColor: {
    color: colors.gray,
  },
  icon: {
    marginRight: dimensions.padding,
  },
  initial: { marginRight: dimensions.padding },
  divider: {
    height: 1,
    backgroundColor: colors.divider,
  },
  chevron: {
    marginHorizontal: dimensions.padding / 2,
  },
  indicator: { marginRight: dimensions.padding },
  rightTexts: {
    flexDirection: 'column',
    marginRight: dimensions.padding,
    alignItems: 'flex-end',
  },
});

const ListItem = ({
  children,
  disabled,
  style,
  icon,
  iconColor,
  initial,
  singleInitial,
  initialColor,
  left,
  right,
  bottom,
  divider,
  chevron,
  onPress,
  big,
  loading,
  rightTexts,
  color,
  dimensions,
  colors,
}) => {
  const styleSheet = _styleSheet({ dimensions, colors });
  const titleStyle = [
    styleSheet.title,
    disabled || loading ? styleSheet.disabledColor : {},
  ];
  const descriptionStyle = [
    ...titleStyle,
    styleSheet.description,
    disabled || loading ? styleSheet.disabledColor : {},
  ];

  const _initialColor = initialColor || colors.blue;

  const defaultColor = disabled || loading ? styleSheet.disabledColor.color : _initialColor;

  const Wrapper = onPress ? TouchableOpacity : View;

  return (
    <Wrapper
      style={[styleSheet.wrapper, style]}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...(onPress ? { onPress } : {})}
    >
      <View style={styleSheet.wrapperInner}>
        {left}
        {initial && (
          <Initials
            name={initial}
            single={singleInitial}
            color={color || defaultColor}
            style={styleSheet.initial}
          />
        )}
        {icon && (
          <Icon
            name={icon}
            size={dimensions.listItemIconSize}
            style={[
              styleSheet.icon,
              iconColor ? { color: color || iconColor } : {},
              disabled || loading ? styleSheet.disabledColor : {},
            ]}
          />
        )}
        {children && (
          <View style={styleSheet.contentWrapper}>
            <View style={styleSheet.contentInner}>
              <View
                style={[
                  styleSheet.textWrapper,
                  big ? styleSheet.textWrapperBig : {},
                  bottom ? styleSheet.textWrapperWithBottom : {},
                ]}
              >
                {typeof children !== 'object' ? (
                  <Text
                    style={[
                      titleStyle,
                      { color },
                      disabled || loading ? styleSheet.disabledColor : {},
                    ]}
                    numberOfLines={1}
                    ellipsizeMode="tail"
                  >
                    {children}
                  </Text>
                ) : (
                  <>
                    {Object.keys(children).includes('type')
                      && React.cloneElement(children, {
                        style: [
                          titleStyle,
                          children.props.style ? children.props.style : {},
                          { color },
                          disabled || loading ? styleSheet.disabledColor : {},
                        ],
                        numberOfLines: 1,
                        ellipsizeMode: 'tail',
                      })}
                    {Object.keys(children).includes('0')
                      && [...children].map((child, index) => React.cloneElement(child, {
                        key: `${child.key}-${Math.random()}`,
                        style: [
                          index === 0 ? titleStyle : descriptionStyle,
                          { color },
                          child.props.style ? child.props.style : {},
                          disabled || loading ? styleSheet.disabledColor : {},
                        ],
                        ...(index === 0
                          ? {
                            numberOfLines: 1,
                            ellipsizeMode: 'tail',
                          }
                          : {}),
                      }))}
                  </>
                )}
              </View>
              {right}
              {loading && (
                <ActivityIndicator
                  color={color || colors.blue}
                  size="small"
                  style={chevron ? {} : styleSheet.indicator}
                />
              )}
              {rightTexts && (
                <View style={styleSheet.rightTexts}>
                  {rightTexts[0] && (
                    <Text style={{ color: color || colors.black }}>
                      {rightTexts[0]}
                    </Text>
                  )}
                  {rightTexts[1] && (
                    <Text
                      style={{
                        color: color || colors.littleDarkGray,
                      }}
                    >
                      {rightTexts[1]}
                    </Text>
                  )}
                </View>
              )}
              {chevron && (
                <Icon
                  name="chevron-right"
                  size={dimensions.listItemIconSize}
                  color={colors.gray}
                  style={styleSheet.chevron}
                />
              )}
            </View>
            {bottom}
            {divider && divider === 'stacked' && (
              <View style={styleSheet.divider} />
            )}
          </View>
        )}
      </View>
      {divider && divider === 'block' && <View style={styleSheet.divider} />}
    </Wrapper>
  );
};

ListItem.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]).isRequired,
  disabled: PropTypes.bool,
  style: ViewPropTypes.style,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  initial: PropTypes.string,
  singleInitial: PropTypes.bool,
  initialColor: PropTypes.string,
  left: PropTypes.node,
  right: PropTypes.node,
  bottom: PropTypes.node,
  divider: PropTypes.oneOf([false, 'block', 'stacked']),
  chevron: PropTypes.bool,
  onPress: PropTypes.func,
  big: PropTypes.bool,
  loading: PropTypes.bool,
  rightTexts: PropTypes.oneOfType([PropTypes.array]),
  color: PropTypes.string,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

ListItem.defaultProps = {
  disabled: false,
  style: {},
  icon: null,
  iconColor: null,
  initial: null,
  singleInitial: true,
  initialColor: null,
  left: null,
  right: null,
  bottom: null,
  divider: 'stacked',
  chevron: false,
  onPress: null,
  big: false,
  loading: false,
  rightTexts: null,
  color: null,
  dimensions: {},
  colors: {},
};

export default ListItem;
