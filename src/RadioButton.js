import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    flexDirection: 'column',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonNotLast: {
    marginBottom: dimensions.padding / 2,
  },
  buttonCicleOuter: {
    marginRight: dimensions.padding,
    borderWidth: 2,
    borderColor: colors.black,
    height: dimensions.padding,
    width: dimensions.padding,
    borderRadius: dimensions.padding / 2,
  },
  checkRadius: { borderRadius: 4 },
  buttonCicleInner: {
    backgroundColor: colors.black,
    height: dimensions.padding / 2,
    width: dimensions.padding / 2,
    margin: (dimensions.padding - dimensions.padding / 2 - 4) / 2,
    borderRadius: dimensions.padding / 4,
  },
  buttonCicleInnerRadius: { borderRadius: 2 },
  label: {
    color: colors.black,
    fontSize: dimensions.inputFontSize,
  },
  selectedBorder: {
    borderColor: colors.blue,
  },
  selectedBackground: {
    backgroundColor: colors.blue,
  },
  selectedColor: {
    color: colors.blue,
  },
  check: {
    height: dimensions.padding - 4,
    width: dimensions.padding - 4,
    backgroundColor: colors.blue,
  },
});

const Button = ({
  selected,
  label,
  value,
  onSelect,
  isLast,
  check,
  style,
  dimensions,
  colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <TouchableOpacity
      style={[styles.button, isLast ? {} : styles.buttonNotLast, style]}
      onPress={() => onSelect(value)}
    >
      <View
        style={[
          styles.buttonCicleOuter,
          selected ? styles.selectedBorder : {},
          check ? styles.checkRadius : {},
        ]}
      >
        {selected && (
          <View
            style={[
              styles.buttonCicleInner,
              selected ? styles.selectedBackground : {},
              check ? styles.buttonCicleInnerRadius : {},
            ]}
          />
        )}
      </View>
      <Text
        style={[styles.label, selected ? styles.selectedColor : {}]}
        numberOfLines={1}
        ellipsizeMode="tail"
      >
        {label}
      </Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  selected: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.any]),
  onSelect: PropTypes.func,
  isLast: PropTypes.bool,
  check: PropTypes.bool,
  style: ViewPropTypes.style,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Button.defaultProps = {
  selected: false,
  label: null,
  value: null,
  onSelect: () => {},
  isLast: false,
  check: false,
  style: {},
  dimensions: {},
  colors: {},
};

/**
 * Verifies if the value is an array when multiple is true
 * @param {*} multiple
 * @param {*} value
 */
const checkMultiVal = (multiple, value) => {
  if (multiple && !Array.isArray(value)) {
    return [];
  }
  return value;
};

/**
 * Check wether the new value exists in array when multiple is true
 * @param {*} value
 * @param {*} newValue
 */
const selectMultiVal = (value, newValue) => {
  let returnValue = [...value];
  if (returnValue.includes(newValue)) {
    returnValue = returnValue.filter((element) => element !== newValue);
  } else {
    returnValue.push(newValue);
  }
  return returnValue;
};

/**
 * Check if the value is selected
 * @param {*} multiple
 * @param {*} values
 * @param {*} value
 */
const checkSelected = (multiple, values, value) => {
  if (multiple) {
    return values.includes(value);
  }
  return values === value;
};

/**
 * Radio Button
 * @param {*} { options: [ { label: "Option", value: 1 } ],
 *  value: 0, light: false, onValueChange: value => console.log(value) }
 */
const RadioButton = ({
  testID,
  multiple,
  options,
  value,
  light,
  onValueChange,
  style,
  buttonStyle,
  dimensions,
  colors,
}) => {
  const [localValue, setLocalValue] = useState(checkMultiVal(multiple, value));

  useEffect(() => {
    setLocalValue(checkMultiVal(multiple, value));
  }, [value, multiple]);

  const styles = _styles({ dimensions, colors });

  return (
    <View style={[styles.wrapper, style]}>
      {options.map((option, index) => (
        <View key={option.value} testID={testID}>
          <Button
            style={buttonStyle}
            check={multiple}
            value={option.value}
            isLast={options.length === index + 1}
            selected={checkSelected(multiple, localValue, option.value)}
            light={light}
            label={option.label}
            onSelect={(newValue) => {
              if (localValue !== newValue || multiple) {
                onValueChange(
                  multiple ? selectMultiVal(localValue, newValue) : newValue,
                );
                setLocalValue(
                  multiple ? selectMultiVal(localValue, newValue) : newValue,
                );
              }
            }}
          />
        </View>
      ))}
    </View>
  );
};

RadioButton.propTypes = {
  multiple: PropTypes.bool,
  options: PropTypes.oneOfType([PropTypes.array]),
  value: PropTypes.oneOfType([PropTypes.any]),
  light: PropTypes.bool,
  onValueChange: PropTypes.func,
  style: ViewPropTypes.style,
  buttonStyle: ViewPropTypes.style,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
  testID: PropTypes.string,
};

RadioButton.defaultProps = {
  multiple: false,
  options: [],
  value: null,
  testID: '',
  light: false,
  onValueChange: () => {},
  style: {},
  buttonStyle: {},
  dimensions: {},
  colors: {},
};

export default withTheme(RadioButton);
