import React from 'react';
import PropTypes from 'prop-types';
import {
  View, Text, StatusBar, SafeAreaView,
} from 'react-native';

import Button from '../Button';

import _styles from './styles';
import { withTheme } from '../utils/theme';

const Header = ({
  navigation,
  title,
  subtitle,
  back,
  left,
  right,
  dimensions,
  colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <>
      <StatusBar backgroundColor={colors.darkBlue} barStyle="default" />
      <SafeAreaView style={styles.wrapper}>
        <View style={styles.container}>
          {left || (
            <Button
              icon={back ? 'arrow-left' : 'menu'}
              primary
              onPress={() => (back ? navigation.goBack() : navigation.toggleDrawer())}
            />
          )}
          <View style={styles.titleWrapper}>
            {title && (
              <Text numberOfLines={1} style={styles.title}>
                {title}
              </Text>
            )}
            {subtitle && (
              <Text numberOfLines={1} style={[styles.title, styles.subtitle]}>
                {subtitle}
              </Text>
            )}
          </View>
          {right}
        </View>
      </SafeAreaView>
    </>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  back: PropTypes.bool,
  left: PropTypes.node,
  right: PropTypes.node,
  navigation: PropTypes.instanceOf(Object),
  dimensions: PropTypes.instanceOf(Object).isRequired,
  colors: PropTypes.instanceOf(Object).isRequired,
};

Header.defaultProps = {
  title: null,
  subtitle: null,
  back: false,
  left: null,
  right: null,
  navigation: {},
};

export default withTheme(Header);
