import { StyleSheet } from 'react-native';

export default ({ colors, dimensions }) => StyleSheet.create({
  wrapper: {
    backgroundColor: colors.darkBlue,
    flexDirection: 'column',
  },
  container: {
    backgroundColor: colors.blue,
    flexDirection: 'row',
    height: dimensions.toolbarHeight,
    alignItems: 'center',
  },
  titleWrapper: {
    flexDirection: 'column',
    paddingHorizontal: dimensions.padding,
    flex: 1,
  },
  title: {
    fontSize: dimensions.toolbarTitleSize,
    color: colors.white,
  },
  subtitle: {
    fontSize: dimensions.toolbarTitleSize * 0.7,
  },
  notification: {
    paddingLeft: dimensions.padding * 2,
  },
});
