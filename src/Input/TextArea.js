/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Input from './index';
import { withTheme } from '../utils/theme';

const TextArea = ({
  inputProps, inputStyle, dimensions, ...props
}) => {
  const height = dimensions.inputHeight - 1;
  const [inputHeight, setInputHeight] = useState(height);

  return (
    <Input
      {...props}
      inputStyle={{
        height: inputHeight,
        ...inputStyle,
      }}
      inputProps={{
        multiline: true,
        onContentSizeChange: (e) => {
          const contSize = e.nativeEvent.contentSize.height;
          setInputHeight(contSize > height ? contSize : height);
        },
        ...inputProps,
      }}
    />
  );
};

TextArea.propTypes = {
  wrapperStyle: PropTypes.oneOfType([PropTypes.object]),
  inputStyle: PropTypes.oneOfType([PropTypes.object]),
  inputProps: PropTypes.oneOfType([PropTypes.object]),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onValueChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.any]),
  before: PropTypes.node,
  after: PropTypes.node,
  error: PropTypes.string,
  success: PropTypes.string,
  info: PropTypes.string,
  light: PropTypes.bool,
  disabled: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
};

TextArea.defaultProps = {
  wrapperStyle: {},
  inputStyle: {},
  inputProps: {},
  placeholder: null,
  label: null,
  onValueChange: () => {},
  value: null,
  before: null,
  after: null,
  error: null,
  success: null,
  info: null,
  light: false,
  disabled: false,
  dimensions: {},
};

export default withTheme(TextArea);
