/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import FeatherIcons from 'react-native-vector-icons/Feather';

import Input from './index';
import { withTheme } from '../utils/theme';

const _styles = ({ dimensions }) => StyleSheet.create({
  toggle: { paddingLeft: dimensions.padding },
});

const Password = ({
  inputProps,
  inputStyle,
  light,
  colors,
  dimensions,
  ...props
}) => {
  const [pinVisible, setPinVisible] = useState(false);

  const styles = _styles({ dimensions });

  return (
    <Input
      {...props}
      light={light}
      inputStyle={{
        ...inputStyle,
      }}
      inputProps={{
        secureTextEntry: !pinVisible,
        keyboardType: 'number-pad',
        ...inputProps,
      }}
      after={(
        <TouchableOpacity
          style={styles.toggle}
          onPress={() => {
            setPinVisible(!pinVisible);
          }}
        >
          <FeatherIcons
            name={pinVisible ? 'eye-off' : 'eye'}
            size={20}
            color={light ? colors.white : colors.gray}
          />
        </TouchableOpacity>
      )}
    />
  );
};

Password.propTypes = {
  wrapperStyle: PropTypes.oneOfType([PropTypes.object]),
  inputStyle: PropTypes.oneOfType([PropTypes.object]),
  inputProps: PropTypes.oneOfType([PropTypes.object]),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onValueChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.any]),
  before: PropTypes.node,
  after: PropTypes.node,
  error: PropTypes.string,
  success: PropTypes.string,
  info: PropTypes.string,
  light: PropTypes.bool,
  disabled: PropTypes.bool,
  colors: PropTypes.oneOfType([PropTypes.object]),
  dimensions: PropTypes.oneOfType([PropTypes.object]),
};

Password.defaultProps = {
  wrapperStyle: {},
  inputStyle: {},
  inputProps: {},
  placeholder: null,
  label: null,
  onValueChange: () => {},
  value: null,
  before: null,
  after: null,
  error: null,
  success: null,
  info: null,
  light: false,
  disabled: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Password);
