/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import {
  View, TextInput, Text, StyleSheet,
} from 'react-native';
import { withTheme } from '../utils/theme';

const _styles = ({ colors, dimensions }) => StyleSheet.create({
  wrapper: {
    width: '100%',
    backgroundColor: colors.white,
    paddingLeft: dimensions.padding,
    marginBottom: dimensions.padding,
  },
  wrapperLight: {
    backgroundColor: colors.blue,
  },
  wrapperDisabled: { opacity: 0.3 },
  inputRow: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingRight: dimensions.padding,
  },
  inputCol: {
    flex: 1,
  },
  textInput: {
    width: '100%',
    height: dimensions.inputHeight - 1,
    color: colors.black,
    fontSize: dimensions.inputFontSize,
    paddingLeft: 0,
  },
  textInputLight: { color: colors.white },
  placeholder: {
    width: '100%',
    position: 'absolute',
    bottom: 1,
    right: 0,
    color: colors.gray,
    lineHeight: dimensions.inputHeight - 1,
  },
  placeholderLight: { color: colors.lightBlue },
  label: {
    height: dimensions.inputFontSize,
    lineHeight: dimensions.inputFontSize,
    fontSize: dimensions.inputFontSize - 2,
    color: colors.black,
  },
  labelList: {
    color: colors.white,
  },
  info: { color: colors.black },
  infoLight: { color: colors.white },
  success: { color: colors.green },
  error: { color: colors.red },
  line: { height: 1, backgroundColor: colors.gray },
  lineLight: { backgroundColor: colors.lightBlue },
  lineFocused: { backgroundColor: colors.black },
  lineLightFocused: { backgroundColor: colors.white },
});

const Input = ({
  wrapperStyle,
  inputStyle,
  inputProps,
  placeholder,
  label,
  onValueChange,
  value,
  before,
  after,
  error,
  success,
  info,
  light,
  disabled,
  onFocus,
  keyboardType,
  textContentType,
  secureTextEntry,
  colors,
  dimensions,
}) => {
  const [focused, setFocused] = useState(false);
  const [localValue, setLocalValue] = useState(value);

  useEffect(() => {
    setLocalValue(value);
  }, [value]);

  const styles = _styles({ colors, dimensions });

  return (
    <View
      style={[
        styles.wrapper,
        light ? styles.wrapperLight : {},
        disabled ? styles.wrapperDisabled : {},
        wrapperStyle,
      ]}
    >
      {label !== null && (
        <Text
          style={[
            styles.textInput,
            styles.label,
            light ? styles.labelList : {},
          ]}
        >
          {label}
        </Text>
      )}
      <View style={styles.inputRow}>
        {before}
        <View style={styles.inputCol}>
          {placeholder !== null && [null, ''].includes(localValue) && (
            <Text
              style={[
                styles.textInput,
                styles.placeholder,
                light ? styles.placeholderLight : {},
              ]}
            >
              {placeholder}
            </Text>
          )}
          <TextInput
            textContentType={textContentType}
            secureTextEntry={secureTextEntry}
            style={[
              styles.textInput,
              light ? styles.textInputLight : {},
              inputStyle,
            ]}
            onChangeText={(v) => {
              setLocalValue(v);
              onValueChange(v);
            }}
            keyboardType={keyboardType}
            value={localValue}
            onFocus={() => {
              setFocused(true);
              onFocus();
            }}
            onBlur={() => setFocused(false)}
            editable={!disabled}
            {...inputProps}
          />
        </View>
        {after}
      </View>
      <View
        style={[
          styles.line,
          light ? styles.lineLight : {},
          focused && light ? styles.lineLightFocused : {},
          focused && !light ? styles.lineFocused : {},
        ]}
      />
      {info !== null && info !== '' && (
        <View>
          <Text
            style={[
              styles.textInput,
              styles.label,
              styles.info,
              light ? styles.infoLight : {},
            ]}
          >
            {info}
          </Text>
        </View>
      )}
      {success !== null && success !== '' && (
        <View>
          <Text style={[styles.textInput, styles.label, styles.success]}>
            {success}
          </Text>
        </View>
      )}
      {error !== null && error !== '' && (
        <View>
          <Text style={[styles.textInput, styles.label, styles.error]}>
            {error}
          </Text>
        </View>
      )}
    </View>
  );
};

Input.propTypes = {
  wrapperStyle: PropTypes.oneOfType([PropTypes.object]),
  inputStyle: PropTypes.oneOfType([PropTypes.object]),
  inputProps: PropTypes.oneOfType([PropTypes.object]),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onValueChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.any]),
  before: PropTypes.node,
  after: PropTypes.node,
  error: PropTypes.string,
  success: PropTypes.string,
  info: PropTypes.string,
  light: PropTypes.bool,
  disabled: PropTypes.bool,
  onFocus: PropTypes.func,
  keyboardType: PropTypes.string,
  textContentType: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  colors: PropTypes.oneOfType([PropTypes.object]),
  dimensions: PropTypes.oneOfType([PropTypes.object]),
};

Input.defaultProps = {
  wrapperStyle: {},
  inputStyle: {},
  inputProps: {},
  placeholder: null,
  label: null,
  onValueChange: () => {},
  value: null,
  before: null,
  after: null,
  error: null,
  success: null,
  info: null,
  light: false,
  disabled: false,
  onFocus: () => {},
  keyboardType: 'default',
  textContentType: 'none',
  secureTextEntry: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Input);
