/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import Input from './index';
import formatPhone from '../utils/formatPhone';
import { withTheme } from '../utils/theme';

const _style = ({ colors, dimensions }) => StyleSheet.create({
  before: {
    color: colors.black,
    paddingRight: dimensions.padding / 2,
    fontSize: dimensions.inputFontSize,
    paddingBottom: 2,
  },
  light: {
    color: colors.white,
  },
});

const Phone = ({
  inputProps, value, light, colors, dimensions, ...rest
}) => {
  const phone = formatPhone(value);

  const [localValue] = useState(phone.ok ? phone.phoneNumber : value);

  const style = _style({ colors, dimensions });

  return (
    <Input
      {...rest}
      light={light}
      before={
        <Text style={[style.before, light ? style.light : {}]}>+250</Text>
      }
      value={localValue}
      inputProps={{
        keyboardType: 'phone-pad',
        testID: 'phoneInput',
        ...inputProps,
      }}
    />
  );
};

Phone.propTypes = {
  wrapperStyle: PropTypes.oneOfType([PropTypes.object]),
  inputStyle: PropTypes.oneOfType([PropTypes.object]),
  inputProps: PropTypes.oneOfType([PropTypes.object]),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onValueChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.any]),
  before: PropTypes.node,
  after: PropTypes.node,
  error: PropTypes.string,
  success: PropTypes.string,
  info: PropTypes.string,
  light: PropTypes.bool,
  disabled: PropTypes.bool,
  colors: PropTypes.oneOfType([PropTypes.object]),
  dimensions: PropTypes.oneOfType([PropTypes.object]),
};

Phone.defaultProps = {
  wrapperStyle: {},
  inputStyle: {},
  inputProps: {},
  placeholder: null,
  label: null,
  onValueChange: () => {},
  value: null,
  before: null,
  after: null,
  error: null,
  success: null,
  info: null,
  light: false,
  disabled: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Phone);
