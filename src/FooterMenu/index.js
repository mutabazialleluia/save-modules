import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import _styles from './styles';
import menu from './menu';
import { withTheme } from '../utils/theme';

const FooterMenu = ({
  navigation, subTitle, colors, dimensions,
}) => {
  const styles = _styles({ colors, dimensions });
  return (
    <View style={styles.container}>
      <View style={styles.footerMenuItemWrapper}>
        {menu.map((item) => (
          <Fragment key={item.name}>
            <TouchableOpacity onPress={() => navigation.navigate(item.screen)}>
              <View
                style={{
                  ...styles.footerMenuItem,
                  ...(subTitle === item.name ? styles.footerActiveMenuItem : {}),
                }}
              >
                {item.icon}
                <Text style={styles.footerMenuItemText} numberOfLines={1}>
                  {item.name}
                </Text>
              </View>
            </TouchableOpacity>
          </Fragment>
        ))}
      </View>
    </View>
  );
};

FooterMenu.defaultProps = {
  subTitle: '',
  dimensions: {},
  colors: {},
};

FooterMenu.propTypes = {
  subTitle: PropTypes.string,
  navigation: PropTypes.oneOfType([PropTypes.object]).isRequired,
  colors: PropTypes.oneOfType([PropTypes.object]),
  dimensions: PropTypes.oneOfType([PropTypes.object]),
};

export default withTheme(FooterMenu);
