import { StyleSheet } from 'react-native';

export default ({ colors, dimensions }) => StyleSheet.create({
  container: {
    backgroundColor: colors.lightPrimary,
    height: 70,
    position: 'absolute',
    top: dimensions.height - 85,
    width: '100%',
  },
  footerMenuItemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footerMenuItem: {
    paddingLeft: 15,
    paddingRight: 10,
    paddingBottom: 5,
    alignItems: 'center',
  },
  footerActiveMenuItem: {
    borderBottomWidth: 5,
    borderBottomColor: colors.white,
  },
  footerMenuItemText: {
    color: colors.white,
    fontSize: 10,
  },
});
