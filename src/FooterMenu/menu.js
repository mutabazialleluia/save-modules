import React from 'react';
import FeatherIcons from 'react-native-vector-icons/Feather';

export default [
  {
    icon: <FeatherIcons name="info" size={25} color="#fff" />,
    name: 'info',
    screen: 'v',
  },
  {
    icon: <FeatherIcons name="users" size={25} color="#fff" />,
    name: 'members',
    screen: 'GroupMembersList',
  },
  {
    icon: <FeatherIcons name="activity" size={25} color="#fff" />,
    name: 'contributions',
    screen: 'UserGroupContributions',
  },
  {
    icon: <FeatherIcons name="inbox" size={25} color="#fff" />,
    name: 'requests',
    screen: 'Requests',
  },
  {
    icon: <FeatherIcons name="pie-chart" size={25} color="#fff" />,
    name: 'shareout',
    screen: 'ShareOut',
  },
];
