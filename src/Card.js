import React from 'react';
import {
  View,
  StyleSheet,
  ViewPropTypes,
  Text,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    borderRadius: 4,
  },
  title: {
    color: colors.blue,
    fontWeight: '800',
    padding: dimensions.padding,
  },
  loading: {
    backgroundColor: colors.whiteSlightlyFaded,
    position: 'absolute',
    height: '100%',
    width: '100%',
    top: 0,
    left: 0,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
  },
});

const Card = ({
  children, style, title, loading, dimensions, colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <View style={[styles.wrapper, style]}>
      {title && (
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
          {title}
        </Text>
      )}
      {children}
      {loading && (
        <View style={styles.loading}>
          <ActivityIndicator color={colors.blue} size="small" />
        </View>
      )}
    </View>
  );
};

Card.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]),
  style: ViewPropTypes.style,
  title: PropTypes.string,
  loading: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Card.defaultProps = {
  children: null,
  style: StyleSheet.create({}),
  title: null,
  loading: false,
  dimensions: {},
  colors: {},
};

export default Card;
