import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native';
import _styles from './styles';
import { withTheme } from '../utils/theme';

const NotFound = ({
  message, imageSrc, dimensions, colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <View style={styles.container}>
      <View style={styles.notFoundPictureWrapper}>
        <Image source={imageSrc} style={styles.notFoundPicture} />
      </View>
      <Text style={styles.notFoundMessage}>{message}</Text>
    </View>
  );
};

NotFound.defaultProps = {
  message: 'Not Found',
  imageSrc: null,
  dimensions: {},
  colors: {},
};

NotFound.propTypes = {
  message: PropTypes.string,
  imageSrc: PropTypes.string,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

export default withTheme(NotFound);
