import { StyleSheet } from 'react-native';

export default ({ dimensions, colors }) => StyleSheet.create({
  container: {
    flex: 1,
    marginTop: dimensions.height / 5,
    padding: 10,
  },
  notFoundPictureWrapper: {
    alignSelf: 'center',
    backgroundColor: colors.gray,
    opacity: 0.2,
    resizeMode: 'center',
    width: 150,
    height: 150,
    marginBottom: 10,
    borderRadius: 150 / 2,
    overflow: 'hidden',
  },
  notFoundPicture: {
    resizeMode: 'center',
    width: 100,
    height: 100,
    alignSelf: 'center',
    marginTop: 25,
  },
  notFoundMessage: {
    textAlign: 'center',
    color: colors.lightGray,
    fontSize: 20,
  },
});
