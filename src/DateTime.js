import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import moment from 'moment';
import Input from './Input';
import { withTheme } from './utils/theme';

/**
 * Display date-time value
 * @param {*} mode
 * @param {*} value
 */
const displayValue = (mode, value) => {
  if (moment(value).isValid()) {
    if (mode === 'date') {
      return moment(value).format('MMMM D, YYYY');
    }
    return moment(value).format('HH:mm');
  }
  return '';
};

/**
 * Return date-time value
 * @param {*} mode
 * @param {*} value
 */
const returnValue = (mode, value) => {
  if (mode === 'date') {
    return moment(value).format('YYYY-MM-DD');
  }
  return moment(value).format('HH:mm');
};

/**
 * Get icon name depending on the mode
 * @param {*} mode
 */
const iconName = (mode) => {
  const names = {
    date: 'calendar',
    time: 'clock',
  };

  return names[mode];
};

/**
 * Validate date
 * @param {*} date
 */
const validateDate = (date) => {
  if (moment(date).isValid()) {
    return moment(date);
  }
  return moment();
};

const DateTime = ({
  value,
  disabled,
  placeholder,
  label,
  mode,
  onValueChange,
  style,
  dimensions,
  colors,
}) => {
  const [show, setShow] = useState(false);
  const [localValue, setLocalValue] = useState(moment(value));

  useEffect(() => {
    setLocalValue(moment(value));
  }, [value]);

  return (
    <>
      <TouchableOpacity
        style={style}
        disabled={disabled}
        onPress={() => setShow(true)}
      >
        <Input
          disabled={disabled}
          inputProps={{
            editable: false,
          }}
          value={displayValue(mode, localValue)}
          placeholder={placeholder}
          label={label}
          after={(
            <Icon
              name={iconName(mode)}
              size={dimensions.padding}
              color={disabled ? colors.gray : colors.black}
            />
          )}
        />
      </TouchableOpacity>
      {show && (
        <DateTimePicker
          value={validateDate(localValue).toDate()}
          mode={mode}
          is24Hour
          display="default"
          onChange={(e, d) => {
            setShow(false);
            if (d) {
              setLocalValue(moment(d));
              onValueChange(returnValue(mode, d));
            }
          }}
        />
      )}
    </>
  );
};

DateTime.propTypes = {
  mode: PropTypes.oneOf(['date', 'time']),
  onValueChange: PropTypes.func,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  style: PropTypes.shape({}),
  placeholder: PropTypes.string,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

DateTime.defaultProps = {
  mode: 'date',
  onValueChange: () => {},
  value: moment().format('YYYY-MM-DD'),
  disabled: false,
  label: null,
  style: {},
  placeholder: null,
  dimensions: {},
  colors: {},
};

export default withTheme(DateTime);
