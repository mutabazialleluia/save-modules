/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Provinces, District, Sector, Cell, Village,
} from 'rwanda';
import Select from './Select';

const label = (name) => ({
  label: name,
  placeholder: name,
});

const Location = ({
  province,
  district,
  sector,
  cell,
  village,
  onValueChange,
  disabled,
}) => {
  const [pr, setPr] = useState(province);
  const [di, setDi] = useState(district);
  const [se, setSe] = useState(sector);
  const [ce, setCe] = useState(cell);
  const [vi, setVi] = useState(village);

  const provinces = [
    ...Provinces().map((p) => ({ label: p, value: p })),
  ];

  let districts = [];
  if (pr) {
    districts = [...District(pr).map((p) => ({ label: p, value: p }))];
  }

  let sectors = [];
  if (di) {
    sectors = [...Sector(pr, di).map((p) => ({ label: p, value: p }))];
  }

  let cells = [];
  if (se) {
    cells = [...Cell(pr, di, se).map((p) => ({ label: p, value: p }))];
  }

  let villages = [];
  if (ce) {
    villages = [
      ...Village(pr, di, se, ce).map((p) => ({ label: p, value: p })),
    ];
  }

  return (
    <>
      <Select
        {...label('province')}
        options={provinces}
        value={pr}
        onValueChange={(value) => {
          setPr(value);
          setDi(null);
          setSe(null);
          setCe(null);
          setVi(null);
          onValueChange(value, 'province');
        }}
        disabled={disabled}
      />
      <Select
        {...label('district')}
        options={districts}
        value={di}
        onValueChange={(value) => {
          setDi(value);
          setSe(null);
          setCe(null);
          setVi(null);
          onValueChange(value, 'district');
        }}
        disabled={disabled}
      />
      <Select
        {...label('sector')}
        options={sectors}
        value={se}
        onValueChange={(value) => {
          setSe(value);
          setCe(null);
          setVi(null);
          onValueChange(value, 'sector');
        }}
        disabled={disabled}
      />
      <Select
        {...label('cell')}
        options={cells}
        value={ce}
        onValueChange={(value) => {
          setCe(value);
          setVi(null);
          onValueChange(value, 'cell');
        }}
        disabled={disabled}
      />
      <Select
        {...label('village')}
        options={villages}
        value={vi}
        onValueChange={(value) => {
          setVi(value);
          onValueChange(value, 'village');
        }}
        disabled={disabled}
      />
    </>
  );
};

Location.propTypes = {
  province: PropTypes.string,
  district: PropTypes.string,
  sector: PropTypes.string,
  cell: PropTypes.string,
  village: PropTypes.string,
  onValueChange: PropTypes.func,
  disabled: PropTypes.bool,
};

Location.defaultProps = {
  province: null,
  district: null,
  sector: null,
  cell: null,
  village: null,
  onValueChange: () => {},
  disabled: false,
};

export default Location;
