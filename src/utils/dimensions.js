import { Dimensions } from 'react-native';

export default {
  padding: 16,
  inputHeight: 40,
  inputFontSize: 14,
  logoSize: 160,
  logoMargin: 24,
  toolbarHeight: 56,
  toolbarTitleSize: 18,
  tabIconSize: 24,
  tabFontSize: 10,
  drawerFontSize: 16,
  listItemFontSize: 16,
  listItemIconSize: 24,
  homeCardFontSize: 13,
  badgeSize: 16,
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
