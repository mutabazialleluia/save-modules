import moment from 'moment';

export const frequencyOptions = ['Daily', 'Weekly', 'Monthly'];

export const lifecycles = [
  { label: '0.5 years', value: '0.5 Years' },
  { label: '1 year', value: '1 Year' },
  { label: '1.5 years', value: '1.5 Years' },
  { label: '2 years', value: '2 Years' },
  { label: '2.5 years', value: '2.5 Years' },
  { label: '3 years', value: '3 Years' },
  { label: '3.5 years', value: '3.5 Years' },
  { label: '4 years', value: '4 Years' },
  { label: '4.5 years', value: '4.5 Years' },
  { label: '5 years', value: '5 Years' },
];
const getNumberWithOrdinal = n => {
  const possiblepostifixes = ['th', 'st', 'nd', 'rd'];
  const v = n % 100;
  return (
    n +
    (possiblepostifixes[(v - 20) % 10] ||
      possiblepostifixes[v] ||
      possiblepostifixes[0])
  );
};

const getPossibleDates = () => {
  const days = [];
  for (let i = 1; i <= 31; i += 1) {
    days.push(getNumberWithOrdinal(i));
  }
  return days;
};

export const weekdays = moment.weekdays();

export const daysOftheMonth = getPossibleDates();
