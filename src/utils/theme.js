import React from 'react';

import colors from './colors';
import dimensions from './dimensions';

const Theme = React.createContext();

export const withTheme = (Component) => {
  class DataComponent extends React.Component {
    render() {
      return (
        <Theme.Consumer>
          {(data) => (
            <Component {...{ colors, dimensions }} {...data} {...this.props} />
          )}
        </Theme.Consumer>
      );
    }
  }

  return DataComponent;
};

export default Theme;
