export default {
  primary: '#199AB1',
  lightPrimary: '#37B6CD',
  darkPrimary: '#167A8C',
  gray: '#cecece',
  littleDarkGray: '#bbbbbb',
  divider: 'rgba(0, 0, 0, 0.12)',
  white: '#ffffff',
  whiteSlightlyFaded: 'rgba(255,255,255,0.9)',
  whiteSemiFaded: 'rgba(255,255,255,0.5)',
  whiteFaded: 'rgba(255,255,255,0.35)',
  black: '#000000',
  blackFaded: 'rgba(0,0,0,0.35)',
  blue: '#199AB1',
  lightBlue: '#62B9C8',
  darkBlue: '#167A8C',
  green: '#5cb85c',
  red: '#a02828',
  lightRed: '#F45B69',
  yellow: '#f0ad4e',
  toolbar: '#37B6CD',
  lightGray: '#B5B5B4',
  darkGray: '#7B6F6F',
  lightGreen: '#68CDA4',
};
