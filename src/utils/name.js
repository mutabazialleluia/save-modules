/**
 * Parse name
 * @param {String} fullName
 */
const parse = fullName => {
  const reName = /(\w+(?:-\w+)?),\s*(\w+)(?:\s+(\w(?=\.)|\w+(?:-\w+)?))?|(\w+)\s+(?:(\w(?=\.)|\w+(?:-\w+)?)\s+)?(\w+(?:-\w+)?)/;

  const matches = fullName.match(reName);

  let first_name = null;
  try {
    first_name = matches[2] || matches[4];
    first_name = first_name || null;
  } catch (exc) {
    // Noting to return
  }

  let middle_name = null;
  try {
    middle_name = matches[3] || matches[5];
    middle_name = middle_name || null;
  } catch (exc) {
    // Noting to return
  }

  let last_name = null;
  try {
    last_name = matches[1] || matches[6];
    last_name = last_name || null;
  } catch (exc) {
    // Noting to return
  }

  if (
    !first_name &&
    !middle_name &&
    !last_name &&
    ![null, '', undefined].includes(fullName)
  ) {
    first_name = fullName.trim();
  }

  return { first_name, middle_name, last_name };
};

/**
 * Display the name
 * @param {Object}  NameObject
 */
const display = ({
  first_name = null,
  middle_name = null,
  last_name = null,
}) => {
  const fullName = [];

  if (![undefined, null, ''].includes(first_name)) {
    fullName.push(first_name);
  }
  if (![undefined, null, ''].includes(middle_name)) {
    fullName.push(middle_name);
  }
  if (![undefined, null, ''].includes(last_name)) {
    fullName.push(last_name);
  }

  return fullName.length === 0 ? '.' : fullName.join(' ');
};

export default { parse, display };
