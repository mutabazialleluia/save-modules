export default (date, format) => {
  let formattedDate = '';
  const month = ((date && new Date(date)) || new Date()).getMonth();
  const day = ((date && new Date(date)) || new Date())
    .getDay()
    .toString();
  const year = ((date && new Date(date)) || new Date()).getFullYear();

  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  if (format) {
    for (let i = 0; i < format.length; i += 1) {
      switch (format[i]) {
        case 'm':
          formattedDate += month + 1;
          break;
        case 'M':
          formattedDate += monthNames[month];
          break;
        case 'D':
          formattedDate += day.length === 1 ? `0${day}` : day;
          break;
        case 'Y':
          formattedDate += year;
          break;
        default:
          formattedDate += format[i];
          break;
      }
    }
  } else {
    formattedDate = new Date(date)
      .toDateString()
      .split(' ')
      .splice(1)
      .join(' ');
  }

  return formattedDate;
};
