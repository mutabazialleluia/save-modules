const passwordError = (password = '', password_confirm = '') => {
  const empty = password.length === 0;
  const short = password.length < 5;

  const mismatch = password !== password_confirm;

  return { empty, short, mismatch };
};
export default passwordError;
