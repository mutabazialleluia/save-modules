export default (
  phone_ = '',
  prefix = 250,
  length = 9,
  starters = [
    { value: '78', label: 'MTN' },
    { value: '72', label: 'Tigo' },
    { value: '73', label: 'Airtel' },
  ],
) => {
  const phone = `${phone_}`.replace(' ', '');
  let label = null;

  if (`${phone}`.length < length) {
    return {
      ok: false,
      phone: `${phone}`,
      phone_: '(+250) 7XX XXX XXX',
      phoneNumber: `${phone}`,
      label,
    };
  }
  const phoneNumber = `${phone}`.substring(`${phone}`.length - length);

  if (
    starters
      .map((s) => {
        const r = phoneNumber.startsWith(`${s.value}`);
        if (r) {
          label = s.label;
        }
        return r;
      })
      .includes(true)
  ) {
    return {
      ok: true,
      phone: `${prefix}${phoneNumber}`,
      phone_: `(+${prefix}) ${phoneNumber.substr(0, 3)} ${phoneNumber.substr(
        3,
        3,
      )} ${phoneNumber.substr(6, 3)}`,
      label,
      phoneNumber,
    };
  }
  return {
    ok: false,
    phone: `${phone}`,
    phone_: '(+250) 7XX XXX XXX',
    label,
    phoneNumber: `${phone}`,
  };
};
