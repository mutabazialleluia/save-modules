import React from 'react';
import {
  View, Text, TouchableOpacity, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const styleSheet = StyleSheet.create({
  wrapper: {
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 16,
  },
  message: {
    fontWeight: 'normal',
    textAlign: 'center',
  },
  action: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

const ActionMessage = ({
  message, action, style, onPress, colors,
}) => (
  <View style={styleSheet.wrapper}>
    <Text style={[styleSheet.message, { color: colors.black }, style]}>
      {message}
    </Text>
    <TouchableOpacity onPress={onPress}>
      <Text style={[styleSheet.action, { color: colors.black }, style]}>
        {action}
      </Text>
    </TouchableOpacity>
  </View>
);

ActionMessage.propTypes = {
  message: PropTypes.string,
  action: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object]),
  onPress: PropTypes.func,
  colors: PropTypes.oneOfType([PropTypes.object]),
};

ActionMessage.defaultProps = {
  message: '',
  action: '',
  style: {},
  onPress: () => {},
  colors: {},
};

export default withTheme(ActionMessage);
