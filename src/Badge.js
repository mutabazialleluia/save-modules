import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const _styles = ({ dimensions, colors }) => StyleSheet.create({
  textWrapper: {
    height: dimensions.badgeSize,
    minWidth: dimensions.badgeSize,
    maxWidth: dimensions.badgeSize * 2,
    borderRadius: dimensions.badgeSize / 2,
    position: 'absolute',
    top: dimensions.padding * 0.25,
    right: dimensions.padding * 0.65,
    paddingHorizontal: dimensions.padding * 0.25,
  },
  text: {
    color: colors.white,
    fontSize: dimensions.badgeSize * 0.75,
    lineHeight: dimensions.badgeSize,
    textAlign: 'center',
  },
});

const Badge = ({
  children, text, color, dimensions, colors,
}) => {
  const styles = _styles({ dimensions, colors });
  return (
    <View>
      {children}
      {!['', 0, null, undefined, '0'].includes(text) && (
        <View
          style={[
            styles.textWrapper,
            { backgroundColor: color || colors.red },
          ]}
        >
          <Text style={styles.text} numberOfLines={1}>
            {text}
          </Text>
        </View>
      )}
    </View>
  );
};

Badge.propTypes = {
  children: PropTypes.node.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Badge.defaultProps = {
  text: 0,
  color: null,
  dimensions: {},
  colors: {},
};

export default withTheme(Badge);
