import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import menu from './menu';
import styles from './styles';
import formatPhone from '../utils/formatPhone';
import Initials from '../Initials';
import Button from '../Button';
import ListItem from '../ListItem';
import { withTheme } from '../utils/theme';
import name from '../utils/name';

const SidebarMenu = ({ navigation, user, lang,colors }) => {
  const { first_name, phone_number } = user;
  return (
    <View style={styles.sideMenuContainer}>
      <View style={styles.sideMenuProfile}>
        <View style={styles.sideMenuProfileTexts}>
          <Initials name={first_name} single />
          <Text
            style={styles.sideMenuProfileName}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {name.display(user)}
          </Text>
          <Text style={styles.sideMenuProfilePhone}>
            {formatPhone(phone_number).phone_}
          </Text>
        </View>
        <Button
          icon="edit-2"
          primary
          small
          onPress={() => navigation.navigate('Profile')}
        />
      </View>
      <ScrollView style={styles.sideMenuItemWrapper}>
        {menu.map((item) => (
          <Fragment key={item.name}>
            <ListItem
              onPress={() => {
                navigation.navigate(item.screen);
                navigation.closeDrawer();
              }}
              icon={item.icon}
              iconColor={item.color ? item.color : null}
              big
              divider={
                Object.keys(item).includes('divider')
                  ? item.divider
                  : 'stacked'
              }
            >
              <Text style={item.color ? { color: item.color } : {}}>
                {lang.t(item.name)}
              </Text>
            </ListItem>
            {item.bottomDivider ? (
              <View style={styles.sideMenuItemDivider} />
            ) : null}
          </Fragment>
        ))}
        <ListItem
          big
          icon="log-out"
          divider={false}
          iconColor={colors.red}
          color={colors.red}
        >
          <Text style={styles.signOut}>{lang.t('sign_out')}</Text>
        </ListItem>
      </ScrollView>
    </View>
  );
};

SidebarMenu.propTypes = {
  navigation: PropTypes.instanceOf(Object).isRequired,
  user: PropTypes.oneOfType([PropTypes.object]).isRequired,
  lang: PropTypes.oneOfType([PropTypes.object]).isRequired,
  colors: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default withTheme(SidebarMenu);
