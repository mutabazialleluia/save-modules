import { StyleSheet } from 'react-native';

export default ({ dimensions, colors }) => StyleSheet.create({
  signOut: { color: colors.red },
  sideMenuContainer: {
    backgroundColor: colors.white,
  },
  sideMenuProfile: {
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
  },
  sideMenuProfileTexts: {
    paddingVertical: dimensions.padding * 2,
    paddingLeft: dimensions.padding,
    flex: 1,
  },
  sideMenuProfileName: {
    fontSize: dimensions.drawerFontSize,
    color: colors.white,
    paddingTop: dimensions.padding,
  },
  sideMenuProfilePhone: {
    fontSize: dimensions.drawerFontSize * 0.8,
    color: colors.white,
  },
  sideMenuItemWrapper: {
    width: '100%',
  },
  sideMenuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: colors.white,
  },
  sideMenuItemText: {
    fontSize: 15,
    marginLeft: 10,
  },
  sideMenuItemIcon: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 20,
  },
  sideMenuItemDivider: {
    width: '100%',
    height: 40,
    backgroundColor: '#F0EFF5',
  },
});
