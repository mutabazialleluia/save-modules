export default [
  {
    icon: 'home',
    name: 'home',
    screen: 'Home',
    divider: false,
    bottomDivider: true,
  },
  {
    icon: 'users',
    name: 'saving_groups',
    screen: 'SavingsGroupList',
  },
  {
    icon: 'user-plus',
    name: 'invitations',
    screen: 'Invites',
    divider: false,
    bottomDivider: true,
  },
  {
    icon: 'settings',
    name: 'settings',
    screen: 'Settings',
  },
];
