import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import PropTypes from 'prop-types';

import Input from './Input';
import Modal from './Modal';
import RadioButton from './RadioButton';
import Button from './Button';
import { withTheme } from './utils/theme';

/**
 * Get the option label to display according to the selected value
 * @param {*} multiple
 * @param {*} options
 * @param {*} value
 */
const newLabel = (multiple, options, value) => {
  let valueLabel = '';

  options.map((option) => {
    if (value === option.value && !multiple) {
      valueLabel = option.label;
    } else if (
      Array.isArray(value)
      && value.includes(option.value)
      && multiple
    ) {
      valueLabel = valueLabel === '' ? option.label : `${valueLabel}, ${option.label}`;
    }
    return option;
  });

  return valueLabel;
};

const formatOptions = (options) => {
  const newOptions = [...options];
  if (typeof options[0] !== 'object') {
    return [
      ...newOptions.map((option) => ({
        value: option,
        label: option,
        key: option,
      })),
    ];
  }

  return newOptions;
};

const Select = ({
  options,
  value,
  disabled,
  label,
  placeholder,
  onValueChange,
  multiple,
  cleanSelectionText,
  doneText,
  dimensions,
  colors,
}) => {
  const [localValue, setLocalValue] = useState(value);
  const [localLabel, setLocalLabel] = useState(
    newLabel(multiple, options, localValue),
  );

  const [inputHeight, setInputHeight] = useState(dimensions.inputHeight - 1);

  useEffect(() => {
    setLocalValue(value);
    setLocalLabel(newLabel(multiple, options, value));
  }, [value, options, multiple]);

  let closeFn = () => {};

  return (
    <Modal
      title={label !== null ? label : 'Choose'}
      headerExtra={
        multiple && (
          <Button
            onPress={() => setLocalValue([])}
            disabled={!Array.isArray(localValue) || localValue.length === 0}
          >
            {cleanSelectionText}
          </Button>
        )
      }
      trigger={(
        <TouchableOpacity disabled={disabled || options.length === 0}>
          <Input
            disabled={disabled || options.length === 0}
            inputStyle={{
              height:
                multiple && [null, ''].includes(localLabel)
                  ? dimensions.inputHeight - 1
                  : inputHeight,
            }}
            inputProps={{
              editable: false,
              multiline: multiple,
              onContentSizeChange: (e) => {
                if (multiple) {
                  const contSize = e.nativeEvent.contentSize.height;
                  setInputHeight(
                    contSize > dimensions.inputHeight
                      ? contSize
                      : dimensions.inputHeight,
                  );
                }
              },
            }}
            value={localLabel}
            placeholder={placeholder}
            label={label}
            after={(
              <Icon
                name="chevron-down"
                size={dimensions.padding}
                color={disabled ? colors.gray : colors.black}
              />
            )}
          />
        </TouchableOpacity>
      )}
      beforeTrigger={() => options.length > 0}
      close={(fn) => {
        closeFn = fn;
      }}
      footer={
        multiple && (
          <Button
            primary
            onPress={() => {
              setLocalLabel(newLabel(true, options, localValue));
              onValueChange(localValue);
              closeFn();
            }}
          >
            {doneText}
          </Button>
        )
      }
    >
      <RadioButton
        multiple={multiple}
        value={localValue}
        options={formatOptions(options)}
        onValueChange={(v) => {
          setLocalValue(v);
          if (!multiple) {
            setLocalLabel(newLabel(false, formatOptions(options), v));
            onValueChange(v);
            closeFn();
          }
        }}
      />
    </Modal>
  );
};

Select.propTypes = {
  options: PropTypes.oneOfType([PropTypes.array]).isRequired,
  value: PropTypes.oneOfType([PropTypes.any]),
  onValueChange: PropTypes.func,
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  cleanSelectionText: PropTypes.string,
  doneText: PropTypes.string,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Select.defaultProps = {
  value: null,
  onValueChange: () => {},
  multiple: false,
  disabled: false,
  label: null,
  placeholder: null,
  cleanSelectionText: 'Clear',
  doneText: 'Done',
  dimensions: {},
  colors: {},
};

export default withTheme(Select);
