import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity, Text, View, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { withTheme } from './utils/theme';

const _styleSheet = ({ dimensions, colors }) => StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.black,
    borderRadius: 4,
  },
  wrapperLight: {
    borderColor: colors.white,
  },
  wrapperDisabled: {
    opacity: 0.3,
  },
  button: {
    height: dimensions.inputHeight * 0.65,
    paddingHorizontal: dimensions.padding / 2,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSelected: {
    backgroundColor: colors.black,
  },
  buttonSelectedLight: {
    backgroundColor: colors.white,
  },
  buttonDisabled: {
    opacity: 0.3,
  },
  separator: {
    height: dimensions.inputHeight * 0.65,
    width: 1,
    backgroundColor: colors.black,
  },
  separatorLight: { backgroundColor: colors.white },
  text: {
    color: colors.black,
    fontSize: dimensions.inputFontSize * 0.85,
  },
  textLight: {
    color: colors.white,
  },
  textSelected: {
    color: colors.white,
  },
  textSelectedLight: {
    color: colors.blue,
  },
});

const Segment = ({
  value,
  onValueChange,
  disabled,
  options,
  style,
  light,
  dimensions,
  colors,
}) => {
  const [localValue, setLocalValue] = useState(value);

  useEffect(() => {
    setLocalValue(value);
  }, [value]);

  const styleSheet = _styleSheet({ dimensions, colors });

  return (
    <View
      style={[
        styleSheet.wrapper,
        disabled ? styleSheet.wrapperDisabled : {},
        light ? styleSheet.wrapperLight : {},
        style,
      ]}
    >
      {options.map((option, index) => (
        <React.Fragment key={option.value}>
          <TouchableOpacity
            style={[
              styleSheet.button,
              option.value === localValue && !light
                ? styleSheet.buttonSelected
                : {},
              option.value === localValue && light
                ? styleSheet.buttonSelectedLight
                : {},
              option.disabled && !disabled ? styleSheet.buttonDisabled : {},
            ]}
            onPress={() => {
              setLocalValue(option.value);
              onValueChange(option.value);
            }}
            disabled={option.disabled || disabled}
          >
            <Text
              style={[
                styleSheet.text,
                light ? styleSheet.textLight : {},
                light ? styleSheet.textLight : {},
                option.value === localValue && !light
                  ? styleSheet.textSelected
                  : {},
                option.value === localValue && light
                  ? styleSheet.textSelectedLight
                  : {},
              ]}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {option.label}
            </Text>
          </TouchableOpacity>
          {options.length !== index + 1 && (
            <View
              style={[
                styleSheet.separator,
                light ? styleSheet.separatorLight : {},
              ]}
            />
          )}
        </React.Fragment>
      ))}
    </View>
  );
};

Segment.propTypes = {
  value: PropTypes.oneOfType([PropTypes.any]),
  onValueChange: PropTypes.func,
  disabled: PropTypes.bool,
  options: PropTypes.oneOfType([PropTypes.array]),
  style: PropTypes.oneOfType([PropTypes.object]),
  light: PropTypes.bool,
  dimensions: PropTypes.oneOfType([PropTypes.object]),
  colors: PropTypes.oneOfType([PropTypes.object]),
};

Segment.defaultProps = {
  value: null,
  onValueChange: () => {},
  disabled: false,
  options: [],
  style: {},
  light: false,
  dimensions: {},
  colors: {},
};

export default withTheme(Segment);
