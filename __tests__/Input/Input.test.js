import React from 'react';
import renderer from 'react-test-renderer';
import Input from '../../src/Input';

describe('Input', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Input />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
