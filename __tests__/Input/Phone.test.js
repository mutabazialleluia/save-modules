import React from 'react';
import renderer from 'react-test-renderer';
import Phone from '../../src/Input/Phone';

describe('Phone', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Phone />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
