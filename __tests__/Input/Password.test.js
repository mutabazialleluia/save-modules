import React from 'react';
import renderer from 'react-test-renderer';
import Password from '../../src/Input/Password';

describe('Password', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Password />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
