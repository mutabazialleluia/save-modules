import React from 'react';
import renderer from 'react-test-renderer';
import TextArea from '../../src/Input/TextArea';

describe('TextArea', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<TextArea />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
