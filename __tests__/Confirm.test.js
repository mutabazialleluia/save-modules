import React from 'react';
import renderer from 'react-test-renderer';
import Confirm from '../src/Confirm';

describe('Confirm', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Confirm />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
