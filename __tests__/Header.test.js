import React from 'react';
import renderer from 'react-test-renderer';
import Header from '../src/Header';

describe('Header', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Header />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
