import React from 'react';
import renderer from 'react-test-renderer';
import { View } from 'react-native';
import Badge from '../src/Badge';

describe('Badge', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Badge><View /></Badge>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
