import React from 'react';
import renderer from 'react-test-renderer';
import Locations from '../src/Locations';

describe('Locations', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Locations />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
