import React from 'react';
import renderer from 'react-test-renderer';
import Select from '../src/Select';

describe('Select', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Select doneText="done" cleanSelectionText="select" options={[]} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
