import React from 'react';
import renderer from 'react-test-renderer';
import Message from '../src/Message';

describe('Message', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Message />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
