import React from 'react';
import renderer from 'react-test-renderer';
import Segment from '../src/Segment';

describe('Segment', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Segment />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
