import React from 'react';
import renderer from 'react-test-renderer';
import DateTime from '../src/DateTime';

describe('DateTime', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<DateTime />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
