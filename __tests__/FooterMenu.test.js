import React from 'react';
import renderer from 'react-test-renderer';
import FooterMenu from '../src/FooterMenu';

describe('Footer Menu', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<FooterMenu navigation={{}} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
