import React from 'react';
import renderer from 'react-test-renderer';
import Skeleton from '../src/Skeleton';

describe('Skeleton', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Skeleton />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
