import React from 'react';
import renderer from 'react-test-renderer';
import RadioButton from '../src/RadioButton';

describe('RadioButton', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<RadioButton />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
