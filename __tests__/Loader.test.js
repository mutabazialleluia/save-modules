import React from 'react';
import renderer from 'react-test-renderer';
import Loader from '../src/Loader';

describe('Loader', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Loader />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
