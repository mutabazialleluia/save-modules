import React from 'react';
import renderer from 'react-test-renderer';
import Card from '../src/Card';

describe('Card', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Card />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
