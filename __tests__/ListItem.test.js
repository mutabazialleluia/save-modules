import React from 'react';
import renderer from 'react-test-renderer';
import { View } from 'react-native';
import ListItem from '../src/ListItem';

describe('ListItem', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<ListItem><View /></ListItem>).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
