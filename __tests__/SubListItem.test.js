import React from 'react';
import renderer from 'react-test-renderer';
import SubListItem from '../src/SubListItem';

describe('SubListItem', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<SubListItem />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
