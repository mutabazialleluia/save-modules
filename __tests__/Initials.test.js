import React from 'react';
import renderer from 'react-test-renderer';
import Initials from '../src/Initials';

describe('Initials', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Initials />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
