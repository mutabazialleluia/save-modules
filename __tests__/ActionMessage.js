import React from 'react';
import renderer from 'react-test-renderer';
import ActionMessage from '../src/ActionMessage';

describe('ActionMessage', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<ActionMessage />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
