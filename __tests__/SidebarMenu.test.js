import React from 'react';
import renderer from 'react-test-renderer';
import SidebarMenu from '../src/SidebarMenu';

describe('SidebarMenu', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<SidebarMenu navigation={{ navigate: jest.fn }} user={{ first_name: '' }} lang={{ t: (e) => e }} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
