import React from 'react';
import renderer from 'react-test-renderer';
import NotFound from '../src/NotFound';

describe('NotFound', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<NotFound imageSrc={null} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
