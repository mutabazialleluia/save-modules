import React from 'react';
import renderer from 'react-test-renderer';
import Modal from '../src/Modal';

describe('Modal', () => {
  it('should render correctly', () => {
    const wrapper = renderer.create(<Modal />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
});
